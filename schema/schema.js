/*
	Author : Brillyan Ilham Akbar
	GraphQL Tutorial for Beginer 
	with NodeJS and Express Framework
*/

const graphql = require('graphql');
const _regex  = require('lodash');

const {
	
	GraphQLObjectType,
	GraphQLID,
	GraphQLString,
	GraphQLInt,
	GraphQLSchema,
	GraphQLList

} = graphql;

/* data dummy */
const Kedai_s = [
	{id: '1', nama: 'Warung Warkop', alamat: 'Jl.Semangka no 12'},
	{id: '2', nama: 'Rumah Makan', alamat: 'Jl.Buah ApaAja no 22'},
	{id: '3', nama: 'Restoran Mahal', alamat: 'Jl.Kiyahi Haji no 9'},
];
const Menu_s = [
	{id: '1', nama: 'Goregan', harga: 2000, kedai_id: '1'},
	{id: '2', nama: 'Lumpia Basa', harga: 10000, kedai_id: '1'},
	{id: '3', nama: 'Kuetar', harga: 5500, kedai_id: '2'},
	{id: '4', nama: 'Bakpao Pait', harga: 3500, kedai_id: '3'},
];

const _kedaiType = new GraphQLObjectType({
	name: 'Kedai',
	fields: () => ({
		id 		: { type: GraphQLID },
		nama	: { type: GraphQLString },
		alamat 	: { type: GraphQLString },
		menus 	: {
			type : GraphQLList(_menuType),
			resolve(parents, args){

				return _regex.filter(Menu_s, { kedai_id: parents.id })
			} 
		}
	})
});

const _menuType = new GraphQLObjectType({
	name 	: 'Menu',
	fields 	: () => ({
		id 			: { type: GraphQLID },
		nama 		: { type: GraphQLString },
		harga 		: { type: GraphQLInt },
	})
})

const _rootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		
		// get by id and with join menu
		kedai: {
			type : _kedaiType,
			args : { id: {type: GraphQLID} },
			resolve(parents, args){

				return _regex.find(Kedai_s, { id: args.id })
			}
		},
		// get all kedai
		kedais : {
			type: new GraphQLList(_kedaiType),
			resolve(parents, args){

				return Kedai_s;
			}
		}
	}
});

module.exports = new GraphQLSchema({
	query: _rootQuery
})