/*
	Author : Brillyan Ilham Akbar
	GraphQL Tutorial for Beginer 
	with NodeJS and Express Framework
*/

const app 		= require('express')();
const graphql 	= require('express-graphql');

/* include schema graphql */
const schema 	= require('./schema/schema');

app.use('/graphql', graphql({

	schema,
	graphiql: true // tools query untuk graphql
}))

app.get('/', (req, res) => {

	res.status(200).json({status: 200, msg: 'Belajar Gayn'})
})

app.listen(1000, () => console.log('Running on Port 1000')); 